package com.world.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MichelController {

    @GetMapping("/hello-to")
    @ResponseBody
    public String getSimplon(@RequestParam(defaultValue = "Simplon")String name){
        return "Hello " + name;
    }

}
